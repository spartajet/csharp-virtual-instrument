﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenHardwareMonitor.Hardware;
using Xunit;
using Xunit.Abstractions;

namespace HardwareTest
{
    public class CpuTemperatureTest
    {
        private readonly ITestOutputHelper outputHelper;

        public CpuTemperatureTest(ITestOutputHelper outputHelper)
        {
            this.outputHelper = outputHelper;
        }

        [Fact]
        public void ReadCpuTemperatureTest()
        {
            UpdateVisitor updateVisitor = new UpdateVisitor();
            Computer computer = new Computer();
            computer.Open();
            computer.CPUEnabled = true;
            for (int k = 0; k < 100; k++)
            {
                computer.Accept(updateVisitor);
                for (int i = 0; i < computer.Hardware.Length; i++)
                {
                    if (computer.Hardware[i].HardwareType == HardwareType.CPU)
                    {
                        this.outputHelper.WriteLine($"CPU Usage: {computer.Hardware[0].Sensors[0].Value}");
                    }
                }

                Thread.Sleep(1000);
            }


            computer.Close();
        }
    }

    public class UpdateVisitor : IVisitor
    {
        public void VisitComputer(IComputer computer)
        {
            computer.Traverse(this);
        }

        public void VisitHardware(IHardware hardware)
        {
            hardware.Update();
            foreach (IHardware subHardware in hardware.SubHardware) subHardware.Accept(this);
        }

        public void VisitSensor(ISensor sensor)
        {
        }

        public void VisitParameter(IParameter parameter)
        {
        }
    }
}