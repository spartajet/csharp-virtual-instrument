﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibreHardwareMonitor.Hardware;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace CpuTemperatureMonitor
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// 温度波形图表Model
        /// </summary>
        private PlotModel temPlotModel;

        /// <summary>
        /// 温度显示Series
        /// </summary>
        private LineSeries tempSeries;

        /// <summary>
        /// UpdateVisitor对象
        /// </summary>
        private UpdateVisitor updateVisitor;

        /// <summary>
        /// OpenHardwareMonitor.Hardware中的Computer对象
        /// </summary>
        private Computer computer;

        /// <summary>
        /// 标记当前系统是否在测量
        /// </summary>
        private bool measureing = false;

        /// <summary>
        /// 测量Task
        /// </summary>
        private Task measureTask;

        /// <summary>
        /// 温度更新委托
        /// </summary>
        /// <param name="temp"></param>
        delegate void UpdateTemp(double temp);

        /// <summary>
        /// 构造函数
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            this.Load += this.MainForm_Load;
        }

        /// <summary>
        /// 窗体加载后事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            this.InitialPlot();
            this.InitialHardware();
        }

        /// <summary>
        /// 测量控制按钮单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlButton_Click(object sender, EventArgs e)
        {
            if ("开始测量".Equals(this.ControlButton.Text))
            {
                //运行开始测量代码
                this.measureing = true;
                this.measureTask = new Task(async () =>
                {
                    while (this.measureing)
                    {
                        double temp = this.ReadCpuTemperature();
                        this.BeginInvoke(new UpdateTemp(OperateNewTemp), temp);

                        //两次采样间隔200ms
                        await Task.Delay(TimeSpan.FromMilliseconds(200));
                    }
                }, TaskCreationOptions.LongRunning);
                this.measureTask.Start();
                this.ControlButton.Text = "结束测量";
            }
            else
            {
                //运行结束测量代码
                this.measureing = false;
                
                this.ControlButton.Text = "开始测量";
            }
        }

        /// <summary>
        /// 处理新采集的温度
        /// </summary>
        /// <param name="temp"></param>
        private void OperateNewTemp(double temp)
        {
            this.tempSeries.Points.Add(new DataPoint(DateTimeAxis.ToDouble(DateTime.Now), temp));
            this.temPlotModel.InvalidatePlot(true);
        }


        /// <summary>
        /// 初始化硬件监控
        /// </summary>
        private void InitialHardware()
        {
            this.updateVisitor = new UpdateVisitor();
            this.computer = new Computer();
            this.computer.Open();
            this.computer.IsCpuEnabled = true;
        }

        /// <summary>
        /// 读取CPU温度
        /// </summary>
        /// <returns></returns>
        private double ReadCpuTemperature()
        {
            this.computer.Accept(updateVisitor);
            ISensor sensor = this.computer.Hardware.First(t => t.HardwareType == HardwareType.Cpu).Sensors
                .First(t1 => t1.Name.Equals("Core Average"));

            //Core Average
            if (sensor == null)
            {
                return -1;
            }
            else
            {
                return sensor.Value ?? -1;
            }
        }

        /// <summary>
        /// 关闭硬件监控
        /// </summary>
        private void CloseHardware()
        {
            this.computer.Close();
        }

        /// <summary>
        /// 初始化波形图表
        /// </summary>
        private void InitialPlot()
        {
            this.temPlotModel = new PlotModel()
            {
                Title = "CPU温度实时图表",
                IsLegendVisible = true,
            };
            this.temPlotModel.Axes.Add(new DateTimeAxis()
            {
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                IntervalLength = 80
            });
            this.temPlotModel.Axes.Add(new LinearAxis()
            {
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                MajorStep = 10,
                MinorStep = 5,
                IntervalLength = 10,
                Minimum = 0,
                Maximum = 60
            });
            this.tempSeries = new LineSeries()
            {
                Color = OxyColors.Blue,
                StrokeThickness = 2,
                MarkerSize = 3,
                MarkerStroke = OxyColors.DarkGreen,
                MarkerType = MarkerType.Diamond,
                Title = "Temp",
            };
            this.temPlotModel.Series.Add(this.tempSeries);
            this.TempPlotView.Model = this.temPlotModel;
        }
    }

    public class UpdateVisitor : IVisitor
    {
        public void VisitComputer(IComputer computer)
        {
            computer.Traverse(this);
        }

        public void VisitHardware(IHardware hardware)
        {
            hardware.Update();
            foreach (IHardware subHardware in hardware.SubHardware) subHardware.Accept(this);
        }

        public void VisitSensor(ISensor sensor)
        {
        }

        public void VisitParameter(IParameter parameter)
        {
        }
    }
}