﻿
namespace CpuTemperatureMonitor
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TempPlotView = new OxyPlot.WindowsForms.PlotView();
            this.ControlButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TempPlotView
            // 
            this.TempPlotView.Dock = System.Windows.Forms.DockStyle.Top;
            this.TempPlotView.Location = new System.Drawing.Point(0, 0);
            this.TempPlotView.Name = "TempPlotView";
            this.TempPlotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.TempPlotView.Size = new System.Drawing.Size(800, 379);
            this.TempPlotView.TabIndex = 0;
            this.TempPlotView.Text = "开始测量";
            this.TempPlotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.TempPlotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.TempPlotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // ControlButton
            // 
            this.ControlButton.Location = new System.Drawing.Point(63, 385);
            this.ControlButton.Name = "ControlButton";
            this.ControlButton.Size = new System.Drawing.Size(84, 41);
            this.ControlButton.TabIndex = 1;
            this.ControlButton.Text = "开始测量";
            this.ControlButton.UseVisualStyleBackColor = true;
            this.ControlButton.Click += new System.EventHandler(this.ControlButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ControlButton);
            this.Controls.Add(this.TempPlotView);
            this.Name = "MainForm";
            this.Text = "CPU温度监控";
            this.ResumeLayout(false);

        }

        #endregion

        private OxyPlot.WindowsForms.PlotView TempPlotView;
        private System.Windows.Forms.Button ControlButton;
    }
}

