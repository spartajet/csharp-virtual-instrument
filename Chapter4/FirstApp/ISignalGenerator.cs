﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApp
{
    internal interface ISignalGenerator
    {
        public double Amplitude { get; set; }
        public double Period { get; set; }
        public double Phase { get; set; }
        public double Offset { get; set; }
    }
}
