﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApp
{
    /// <summary>
    /// 信号发生器抽象类
    /// </summary>
    public abstract class SignalGenerator
    {
        /// <summary>
        /// 幅值
        /// </summary>
        public double Amplitude { get; set; } = 1;

        /// <summary>
        /// 周期
        /// </summary>
        public double Period { get; set; } = 2 * Math.PI;

        /// <summary>
        /// 相位
        /// </summary>
        public double Phase { get; set; } = 0;

        /// <summary>
        /// 偏移
        /// </summary>
        public double Offset { get; set; } = 0;

        /// <summary>
        /// 当前的相位
        /// </summary>
        protected double currentPhase = 0;

        /// <summary>
        /// 获取波形数据
        /// </summary>
        /// <param name="sampleCount">采样数</param>
        /// <returns>波形数据</returns>
        public abstract double[] AcquireData(int sampleCount);
        /// <summary>
        /// 重置当前的相位
        /// </summary>
        public void ResetCurrentPhase()
        {
            this.currentPhase = 0;
        }
    }
}